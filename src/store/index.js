import system from './modules/system/index.js'
import root from './modules/root/index.js'
import empresas from './modules/empresas/index.js'
import associacao from './modules/associacao/index.js'
import cidades from './modules/cidades/index.js'
import noticias from './modules/noticias/index.js'
export default {
	modules: {
		system, root, empresas, associacao, cidades, noticias
	}
}
