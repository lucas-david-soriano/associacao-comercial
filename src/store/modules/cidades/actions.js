export default {

    listaEstado({commit, getters, rootState})
      {
        return new Promise( (res,rej) => {
          getters.getApi.get(`estado`).then( resp => {
            commit('SET_ESTADO', resp.data.data)
            res(resp.data)
          })
        })
    },
  
    cadastroEstado ({getters, commit, rootState}, data) 
    {
      return new Promise( (res,rej) => {
        getters.getApi.post(`estado`, data).then( resp => {
          res(resp.data)
        })
      })
    },
  
    alteraEstado({commit, getters, rootState}, data)
      {
      return new Promise( (res,rej) => {
          getters.getApi.put(`estado`, data).then( resp => {
            res(resp.data)
          })
      })
    },
  
    deletaEstado({commit, getters, rootState}, estado)
      {
      return new Promise( (res,rej) => {
            getters.getApi.delete(`estado/${estado._id}`).then( resp => {
              res(resp.data)
            })
      })
    },
  
    listaCidade({commit, getters, rootState})
      {
      return new Promise( (res,rej) => {
          getters.getApi.get(`v1/cidades`).then( resp => {
            
            commit('SET_CIDADE', resp.data)
            res(resp.data)
          })
      })
    },
  
    cadastroCidade ({getters, commit, rootState}, data) 
    {
      return new Promise( (res,rej) => {
        getters.getApi.post(`v1/cidades`, data).then( resp => {
          res(resp.data)
        })
      })
    },
  
    alteraCidade({commit, getters, rootState}, data)
      {
      return new Promise( (res,rej) => {
          getters.getApi.put(`v1/cidades`, data).then( resp => {
            res(resp.data)
          })
      })
    },
  
    deletaCidade({commit, getters, rootState}, cidade)
      {
      return new Promise( (res,rej) => {
            getters.getApi.delete(`v1/cidades/${cidade._id}`).then( resp => {
              res(resp.data)
            })
      })
    },

    addCidadeImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.post(`v1/cidades/addimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    removeCidadeImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.put(`v1/cidades/removeimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },
  }
  