export default {
    listaUsuarios({commit, getters})
      {
        return new Promise( (res,rej) => {
          getters.getApi.get(`v1/user`).then( resp => {
            commit('SET_USUARIOS', resp.data)
            res(resp.data)
          })
        })
    },

    listaUsuariosAssociacao({commit, getters}, data)
      {
        return new Promise( (res,rej) => {
          getters.getApi.get(`v1/user/${data}`).then( resp => {
            commit('SET_USUARIOS', resp.data)
            res(resp.data)
          })
        })
    },
  
    cadastraUsuario({commit, getters}, usuario)
      {
        return new Promise( (res,rej) => {
          getters.getApi.post(`v1/user`, usuario).then( resp => {
            res(resp.data)
          })
        })
    },
  
    alteraUsuario({commit, getters}, usuario)
      {
        return new Promise( (res,rej) => {
          getters.getApi.put(`v1/user`, usuario).then( resp => {
            res(resp.data)
          })
        })
    },
  
    removeUsuario({commit, getters}, usuario_id)
      {
          return new Promise( (res,rej) => {
            getters.getApi.delete(`v1/user/${usuario_id}`).then( resp => {
              res(resp.data)
            })
          })
    },
  }
  