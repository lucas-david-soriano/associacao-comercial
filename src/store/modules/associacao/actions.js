export default {
    listaAssociacao ({getters, commit}) {
        return new Promise( (res,rej) => {
          getters.getApi.get(`v1/associacoes`).then( resp => {
            commit('SET_ASSOCIACAO', resp.data)
            res(resp.data)
          })
        })
    },

    saveAssociacao({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.post(`v1/associacoes`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    addAssociacaoImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.post(`v1/associacoes/addimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    removeAssociacaoImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.put(`v1/associacoes/removeimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    alteraAssociacao({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.put(`v1/associacoes`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    deletaAssociacao({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.delete(`v1/associacoes/${data}`).then( resp => {
          res(resp.data)
        })
      })
    }
}
