export default {
    listaNoticias({commit, getters}, data)
    {
        return new Promise( (res,rej) => {
            getters.getApi.get(`v1/noticias/${data}`).then( resp => {
              commit('SET_NOTICIAS', resp.data)
              res(resp.data)
            })
      })
    },

    salvaNoticia({commit, getters}, data)
    {
        console.log(data)
        return new Promise( (res,rej) => {
            getters.getApi.post(`v1/noticias`, data).then( resp => {
              console.log(resp)
              res(resp.data)
            })
      })
    },

    alteraNoticia({commit, getters}, data)
    {
        return new Promise( (res,rej) => {
            getters.getApi.put(`v1/noticias`, data).then( resp => {
              res(resp.data)
            })
      })
    },

    deletaNoticia({commit, getters}, data)
    {
        return new Promise( (res,rej) => {
            getters.getApi.delete(`v1/noticias/${data}`).then( resp => {
              res(resp.data)
            })
      })
    },

    pegaImagem({commit}, data)
    {
      commit('SET_IMAGEM', data)
    },


    addNoticiaImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.post(`v1/noticias/addimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },

    removeNoticiaImage({getters, commit}, data) {
      return new Promise( (res,rej) => {
        getters.getApi.put(`v1/noticias/removeimage`, data).then( resp => {
          res(resp.data)
        })
      })
    },
}