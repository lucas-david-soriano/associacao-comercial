export default {
    'SET_NOTICIAS' (state, noticias) {
        state.noticias = noticias
    },

    'SET_IMAGEM' (state, imagem) {
        state.imagem.push(imagem)
    }
}