export default {
  getEmpresas: state => state.empresas,
  getCategoriasEmpresa: state=> state.categoriasempresa,
  getEmpresasTotal: state => state.empresas.length,
  getCategorias: state => state.categorias
}
