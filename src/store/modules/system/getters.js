export default {
    getUser: state => {return state.user},
    getToken: state => {return state.token},
    getApi: state => {return state.$api},
    getServidor: state => state.servidor,
    getIsLoadingCheckUser: state => {return state.isLoadingCheckUser},
    getDataUser: state => {return state.dataUser},
    getEmpresaSelecionada: state => {return state.empresaSelecionada}
}
