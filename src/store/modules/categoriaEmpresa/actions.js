export default {
  loadCategoriasEmpresas({commit, getters}) {
    return new Promise( (res,rej) => {
      getters.getApi.get(`categoriaempresa`).then( resp => {
        commit('SET_CATEGORIAS_EMPRESAS', resp.data.data )
        res(resp.data.status)
      })
    })
  }
}
