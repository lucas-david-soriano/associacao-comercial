import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../helpers/Auth'
import VerifyToken from '../helpers/VerifyToken'
import Login from '../components/Login'
import Home from '../components/Home'
import Empresas from '../components/user/empresas/Index'
import InfoAssociacao from '../components/user/associacao/InfoAssociacao'
import CidadeEstado from '../components/user/cidade-estado/CidadeEstado.vue'
import Cidades from '../components/user/cidade-estado/Cidades'
import Estados from '../components/user/cidade-estado/Estados'
import Usuarios from '../components/user/usuarios/Usuarios.vue'
import Noticias from '../components/user/noticias/Noticias.vue'
import Error404 from '../components/Error404'

Vue.use(Router)

export default new Router({
  routes: [
    {
        path: '/',
        redirect:"/login"
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        beforeEnter:VerifyToken
    },
    {
        path: '/home',
        name: 'home',
        component: Home,
        beforeEnter:Auth
    },
    {
      path: '/empresas',
      name: 'empresas',
      component: Empresas,
      beforeEnter:Auth
    },
    {
        path: '/associacao',
        name: 'associacao',
        component: InfoAssociacao,
        beforeEnter:Auth
    },

    {
        path: '/cidade-estado',
        name: 'cidade-estado',
        component: CidadeEstado,
        redirect:'/gerenciacidades',
        beforeEnter:Auth,
        children:[
            {
                path: '/gerenciacidades',
                name: 'gerenciacidades',
                component: Cidades,
                beforeEnter:Auth
            },
            {
                path: '/gerenciaestados',
                name: 'gerenciaestados',
                component: Estados,
               beforeEnter:Auth
            }
        ]
    },
    {
        path: '/usuarios',
        name: 'usuarios',
        component: Usuarios,
        beforeEnter:Auth
    },
    {
        path: '/noticias',
        name: 'noticias',
        component: Noticias,
        beforeEnter:Auth
    },
   /* {
        path: '/produtos',
        name: 'produtos',
        component: Produtos,
        //beforeEnter:Auth
    },
    {
        path: '/pedidos',
        name: 'pedidos',
        component: Pedidos,
        redirect:'/pedidosativos',
        ////beforeEnter:Auth,
        children:[
            {
                path: '/pedidosativos',
                name: 'pedidosativos',
                component: PedidosAtivos,
                ////beforeEnter:Auth
            },
            {
                path: '/pedidosarquivados',
                name: 'pedidosarquivados',
                component: PedidosArquivados,
                ////beforeEnter:Auth
            }
        ]
    },
    {
        path: '/ofertas',
        name: 'ofertas',
        component: Ofertas,
        ////beforeEnter:Auth
    },*/
    /*{
        path: '/chat',
        name: 'chatroot',
        component: ChatRoot,
        ////beforeEnter:Auth,
        children:[
            {
                path: '/chat/:id',
                name: 'chat',
                component: Chat,
                ////beforeEnter:Auth
            }
        ]
    },*/
   /* {
        path: '/indicadores',
        name: 'indicadores',
        component: Indicadores,
        ////beforeEnter:Auth
    },
    {
        path: '/meusdados',
        name: 'meusdados',
        component: MeusDados,
        ////beforeEnter:Auth
    },*/
    {
        path: '*',
        name: '404',
        component: Error404
    }
  ]
})
