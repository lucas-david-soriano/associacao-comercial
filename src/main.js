import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import Vuex from 'vuex'
import router from './router'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import EventHub from 'vue-event-hub'
import VueQuillEditor from 'vue-quill-editor'
import VueClip from 'vue-clip'
import VueTheMask from 'vue-the-mask'
import {vueImgPreview} from 'vue-img-preview'



// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'
Vue.use(Vuetify, {
  iconfont: 'fa4'
})
Vue.use(VueQuillEditor, /* { default global options } */)
Vue.use(VueTheMask)
Vue.use(EventHub)
Vue.use(VueChartkick, {adapter: Chart})
Vue.use(Vuex)
Vue.use(Vuetify)
Vue.use(VueClip)

Vue.component('vue-img-preview', vueImgPreview)

import 'vuetify/dist/vuetify.min.css'

import VuexStore from './store'
const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api = axios.create({
  //baseURL: 'https://api.bigoferta.com/'
  baseURL: state.servidor
})


//config Authorization
state.$api.interceptors.request.use( config => {
    config.headers.common['Authorization'] = state.token
    console.log(config)
    return config
}, error => {
    return Promise.reject(error)
})

/*
//config Authorization
state.$api.interceptors.response.use( resp => {
    return resp
}, error => {
    if( error.response.status == 403 )
        sessionStorage.removeItem("token")
    return Promise.reject(error)
})
*/

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
